<?php

/**
 * Facebook OAuth action callback; get pages user that is an admin of.
 */
function social_dashboard_fb_graph_pages($app_id, $access_token) {
  global $user;
  $result = fboauth_graph_query('me/accounts', $access_token);

  foreach ($result->data as $account) {
    $accounts[$account->id] = $account->name;
    $pagedata = fboauth_graph_query($account->id, $access_token);
    cache_set('social_fb_page_' . $account->id, (array)$pagedata);
  }

  cache_set('social_fb_accounts_' . $user->uid, $accounts);

  return 'social/facebook/pages';
}

/**
 * Facebook OAuth action callback; get page data.
 */
function social_dashboard_fb_graph_data($app_id, $access_token) {
  global $user;
  $pageid = cache_get('social_fb_queue_' . $user->uid);
  
  $result = fboauth_graph_query($pageid->data, $access_token);
  
  cache_set('social_fb_page_' . $pageid->data, (array)$result);
  
  return 'social/facebook/page/' . $pageid->data;
}

/**
 * Facebook OAuth action callback; get page data.
 */
function social_dashboard_fb_graph_posts($app_id, $access_token) {
  global $user;
  $pageid = cache_get('social_fb_queue_' . $user->uid);
  
  $result = fboauth_graph_query($pageid->data . '/posts?limit=500&since=' . strtotime('-30 days'), $access_token);
  
  foreach ($result->data as $post) {
    if (isset($post->message)) {
      $post_insights = fboauth_graph_query($post->id . '/insights/post_story_adds_by_action_type', $access_token);
      
      $share_count = isset($post->shares->count) ? $post->shares->count : 0;
      $like_count = isset($post->likes->count) ? $post->likes->count : 0;
      $share_count_all = isset($post_insights->data[0]->values[0]->value->share) ? $post_insights->data[0]->values[0]->value->share : 0;
      $like_count_all = isset($post_insights->data[0]->values[0]->value->like) ? $post_insights->data[0]->values[0]->value->like : 0;
      $comment_count_all = isset($post_insights->data[0]->values[0]->value->comment) ? $post_insights->data[0]->values[0]->value->comment : 0;
  
      $posts[$post->id] = array(
        'created_time' => $post->created_time,
        'message' => $post->message,
        'shares' => array('page' => $share_count, 'all' => $share_count_all),
        'likes' => array('page' => $like_count, 'all' => $like_count_all),
        'comments' => array('all' => $comment_count_all),
      );
    }
  }
  
  cache_set('social_fb_page_' . $pageid->data . '_posts', $posts);
  
  return 'social/facebook/page/' . $pageid->data;
}

